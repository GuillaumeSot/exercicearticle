﻿// See https://aka.ms/new-console-template for more information
namespace ExerciceArticle
{
    public class Programme
    {
        static void Main()
        {
            bool existe = false;
            Article art1 = new Article() { prix = 5, nomArticle = "eeeee", reference = "b441" };
            Article a1 = new Article() { prix = 1, nomArticle = "aaaaaa", reference = "a346" };
            List<Article> stock = new List<Article>();
            stock.Add(art1);
            stock.Add(a1);
            Console.WriteLine("1) Rechercher un article par référence. 2) Ajouter un article au stock en vérifiant l’unicité de la référence. 3) Supprimer un article par référence. 4) Modifier un article par référence. 5) Rechercher un article par nom. 6) Rechercher un article par intervalle de prix de vente. 7) Afficher tous les articles. 8) Quitter");
            string choix = Console.ReadLine();
            switch (choix)
            {
                case "1":
                    Console.WriteLine("reference:");
                    string reference = Console.ReadLine();
                    foreach (Article unArticle in stock)
                    {
                        if (unArticle.reference == reference)
                        {
                            Console.WriteLine($" ref: {unArticle.reference} nom: {unArticle.nomArticle} prix: {unArticle.prix} ");
                            existe = true;
                        }
                    }
                    if(existe == false)
                    {
                        Console.WriteLine("l'article n'existe pas");
                    }
                    break;
                case "2":
                    Console.WriteLine("reference");
                    reference = Console.ReadLine();
                    foreach (Article unArticle in stock)
                    {
                        if (unArticle.reference == reference)
                        {
                            Console.WriteLine("l'article existe déjà");
                             existe = true;
                            break;
                        }
                    }
                    if (existe == false)
                    {
                        Article article = new Article();
                        article.reference = reference;

                        Console.WriteLine("nom");
                        string nom = Console.ReadLine();

                        article.nomArticle = nom;

                        Console.WriteLine("prix");
                        int prix = Int32.Parse(Console.ReadLine());

                        article.prix = prix;
                        Console.WriteLine("l'article a été ajouté avec succès");
                    }
                    break;

                    case "3":
                    Console.WriteLine("reference:");
                    reference = Console.ReadLine() ;
                    foreach (Article unArticle in stock)
                    {
                        if (unArticle.reference == reference)
                        {
                            stock.Remove(unArticle);
                            Console.WriteLine("l'article a été supprimé");
                            foreach(Article unArticle2 in stock)
                            {
                                Console.WriteLine($"liste des article restant{unArticle2.nomArticle}");
                            }
                        }
                    }
                    break;

                    case "4":
                    Console.WriteLine("reference:");
                    reference = Console.ReadLine();
                    foreach (Article unArticle in stock)
                    {
                        if (unArticle.reference == reference)
                        {
                            Console.WriteLine("changer le nom de l'article : ");
                            string nom = Console.ReadLine();
                            unArticle.nomArticle = nom;
                            Console.WriteLine("changer le prix de l'article : ");
                            int prix = Int32.Parse(Console.ReadLine());
                            unArticle.prix = prix;
                            existe = true;
                            Console.WriteLine("l'article a été modifié avec succès");

                        }
                    }
                    if(existe == false)
                    {
                        Console.WriteLine("l'article n'existe pas");
                    }

                        break;
                    case "5":
                    Console.WriteLine("nom:");
                    string nomArt = Console.ReadLine();
                    foreach (Article unArticle in stock)
                    {
                        if (unArticle.nomArticle == nomArt)
                        {
                            Console.WriteLine($" ref: {unArticle.reference} nom: {unArticle.nomArticle} prix: {unArticle.prix} ");
                            existe = true;
                        }
                    }
                    if (existe == false)
                    {
                        Console.WriteLine("l'article n'existe pas");
                    }

                    break;

                    case "6":
                    Console.WriteLine("min");
                    int min = Int32.Parse(Console.ReadLine());

                    Console.WriteLine("max");
                    int max = Int32.Parse(Console.ReadLine());

                    foreach (Article unArticle in stock)
                    {
                        if (unArticle.prix>= min && unArticle.prix<= max)
                        {
                            Console.WriteLine($" ref: {unArticle.reference} nom: {unArticle.nomArticle} prix: {unArticle.prix} ");
                        }
                    }
                    break;

                case "7":
                    foreach (Article unArticle in stock)
                    {
                            Console.WriteLine($" ref: {unArticle.reference} nom: {unArticle.nomArticle} prix: {unArticle.prix} ");

                    }

                    break;
            }
        }
    }
}