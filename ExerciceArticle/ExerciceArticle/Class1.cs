﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExerciceArticle
{
    public class Article
    {
        public string reference { get; set; }
        public string nomArticle { get; set; }
        public int prix { get; set; }


        public override string ToString()
        {
            return $"{base.ToString()} {reference.ToString()} {nomArticle.ToString()} {prix.ToString()}";
        }
    }
}
